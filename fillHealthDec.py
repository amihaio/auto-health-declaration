from selenium import webdriver
import json, time


def run_dec_filler():
    DRIVER_PATH = '/Users/amihai/Documents/chromedriver'
    driver = webdriver.Chrome(DRIVER_PATH)

    driver.get('https://lp.landing-page.mobi/index.php?page=landing&id=332721&token=837e0653b38856006c807d87e4b076db')

    with open('config.json', 'r') as f:
        config = json.load(f)

    driver.find_element_by_id('form_8780611_field_0').send_keys('עמיחי אופנבכר')

    driver.find_element_by_id('form_8780611_field_1').send_keys(config['phone'])

    driver.find_element_by_id('form_8780611_field_2').send_keys(config['id'])

    campus = driver.find_element_by_name("field[3]")
    for option in campus.find_elements_by_tag_name('option'):
        if option.text == 'א.י. ספרא':
            option.click()
            break

    personnel = driver.find_element_by_name("field[4]")
    for option in personnel.find_elements_by_tag_name('option'):
        if option.text == 'סטודנט':
            option.click()
            break

    temp = driver.find_element_by_name("field[5]")
    for option in temp.find_elements_by_tag_name('option'):
        if option.text == 'כן- מתחת ל 38':
            option.click()
            break

    driver.find_element_by_name("field[6][0]").click()

    driver.find_element_by_xpath("//*[@id='form_8780611']/input[10]").click()


if __name__ == "__main__":
    run_dec_filler()