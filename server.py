from flask import Flask, render_template
from fillHealthDec import run_dec_filler

app = Flask(__name__)


@app.route("/")
def home():
    run_dec_filler()
    return "filled health declaration"


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=False)