from selenium import webdriver
import json, time
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By


def run_yad2():
    DRIVER_PATH = '/Users/amihai/Documents/chromedriver'
    driver = webdriver.Chrome(DRIVER_PATH)

    # driver.get('https://my.yad2.co.il/login.php')
    driver.get('https://my.yad2.co.il/newOrder/index.php?action=personalAreaFeed&CatID=2&SubCatID=2')
    # driver.get('https://theverge.com')

    driver.find_element_by_id('userName').send_keys('hodayamoyalmor@gmail.com')
    driver.find_element_by_id('password').send_keys('verna1234')
    driver.find_element_by_id('submitLogonForm').click()

    time.sleep(6)
    driver.find_element_by_link_text('נדלן דירות להשכרה (1)').click()

    close_popup_1(driver)
    close_popup_2(driver)

    time.sleep(5)

    try:
        WebDriverWait(driver,10).until(
            EC.frame_to_be_available_and_switch_to_it((By.XPATH, '//*[@id="feed"]/tbody/tr[3]/td/iframe'))
        )
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="bounceRatingOrderBtn"]'))
        )
        element.click()
        # driver.find_element_by_xpath('//*[@id="order_form"]/div[2]/div[2]/div[2]/div[2]/div/div/div[2]/div').click()
    except Exception as e:
        print(f'could not click on button: {str(e)}')
        close_popup_1(driver)
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.XPATH, '//*[@id="bounceRatingOrderBtn"]'))
        )
        element.click()

def close_popup_1(driver):
    try:
        element = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable((By.ID, 'Layer_1'))
        )
        element.click()
    except:
        print('no pop up num 1')

def close_popup_2(driver):
    try:
        element = WebDriverWait(driver, 10).until(
            # EC.element_to_be_clickable((By.CLASS_NAME, 'closeToolTipIframe'))
            EC.element_to_be_clickable((By.XPATH, '//*[@id="sLightbox_container"]/div/div[1]'))
        )
        element.click()
    except:
        print('no pop up num2')
        # driver.find_element_by_class_name('closeToolTipIframe').click()
    driver.find_element_by_xpath('//*[@id="feed"]/tbody/tr[2]').click()


if __name__ == '__main__':
    run_yad2()
